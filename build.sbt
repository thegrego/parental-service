lazy val commonSettings = Seq(
  name := "sky",
  version := "0.1",
  scalaVersion := "2.12.8"
)

lazy val libs = Seq(
  "org.scalactic" %% "scalactic" % "3.0.5",
  "org.scalatest" %% "scalatest" % "3.0.5" % Test,
  "org.scalamock" %% "scalamock-scalatest-support" % "3.6.0" % Test
)

lazy val sky = (project in file(".")).
  settings(commonSettings: _*).
  settings(
    libraryDependencies ++= libs
  )
