package sky.domain

import org.scalatest.WordSpec

class ParentalLevelSpec extends WordSpec {
  "Parental Level" should {
    "return instance of level U with name 'U' and weight 100" in {
      assert(ParentalLevel("U") == U("U", 100))
      assert(ParentalLevel("u") == U("U", 100))
    }

    "return instance of level PG with name 'PG' and weight 200" in {
      assert(ParentalLevel("PG") == PG("PG", 200))
      assert(ParentalLevel("pg") == PG("PG", 200))
    }

    "return instance of level Twelve with name '12' and weight 300" in {
      assert(ParentalLevel("12") == Twelve("12", 300))
    }

    "return instance of level Fifteen with name '15' and weight 400" in {
      assert(ParentalLevel("15") == Fifteen("15", 400))
    }

    "return instance of level Eighteen with name '18' and weight 500" in {
      assert(ParentalLevel("18") == Eighteen("18", 500))
    }

    "return instance of level Undefined with name 'foo' and weight Int.MaxValue" in {
      assert(ParentalLevel("foo") == Undefined("foo", Int.MaxValue))
    }
  }

}
