package sky.service

import scala.util.{Failure, Success}

import org.scalamock.scalatest.MockFactory
import org.scalatest.{Matchers, WordSpec}
import sky.exception.{MovieNotAvailableException, TitleNotFoundException}

class ParentalControlServiceProdSpec extends WordSpec with Matchers with MockFactory {

  val movieService: MovieService = mock[MovieService]

  val parentalControlService = new ParentalControlService(movieService)

  "Parental Control Service" should {
    "return Success(true) if movie exists and customer parental level is equal to movie parental level" in {
      // given
      val movieId = "1"
      val movieParentalLevel = "U"
      val customerParentalLevel = "U"

      movieService.getParentalControlLevel _ expects movieId returns Success(movieParentalLevel)

      // when
      val result = parentalControlService.movieAllowed(customerParentalLevel, movieId)

      // then
      assert(result === Success(true))
    }

    "return Success(true) if movie exists and customer parental level is greater then movie parental level" in {
      // given
      val movieId = "1"
      val movieParentalLevel = "12"
      val customerParentalLevel = "15"

      movieService.getParentalControlLevel _ expects movieId returns Success(movieParentalLevel)

      // when
      val result = parentalControlService.movieAllowed(customerParentalLevel, movieId)

      // then
      assert(result === Success(true))
    }

    "return Success(false) if movie exists and customer parental level is less then movie parental level" in {
      // given
      val movieId = "1"
      val movieParentalLevel = "18"
      val customerParentalLevel = "15"

      movieService.getParentalControlLevel _ expects movieId returns Success(movieParentalLevel)

      // when
      val result = parentalControlService.movieAllowed(customerParentalLevel, movieId)

      // then
      assert(result === Success(false))
    }

    "return Failure(TitleNotFoundException) if movie does not exist" in {
      // given
      val movieId = "10"
      val customerParentalLevel = "15"
      val failure = Failure(new TitleNotFoundException(movieId))

      movieService.getParentalControlLevel _ expects movieId returns failure

      // when
      val result = parentalControlService.movieAllowed(customerParentalLevel, movieId)

      // then
      assert(result === failure)
    }

    "return Failure(MovieNotAvailableException) in case of TechnicalServiceException in MovieService" in {
      // given
      val movieId = "10"
      val customerParentalLevel = "15"
      val failure = Failure(new MovieNotAvailableException)

      movieService.getParentalControlLevel _ expects movieId returns failure

      // when
      val result = parentalControlService.movieAllowed(customerParentalLevel, movieId)

      // then
      assert(result === failure)
    }
  }
}
