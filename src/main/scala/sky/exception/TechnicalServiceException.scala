package sky.exception

class TechnicalServiceException extends Exception("The movie is not available due to technical issues.")
