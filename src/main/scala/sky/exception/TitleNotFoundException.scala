package sky.exception

class TitleNotFoundException (val movieId: String) extends Exception(s"The movie service could not find the movie for the id: $movieId")
