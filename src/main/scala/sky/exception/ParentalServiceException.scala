package sky.exception

sealed trait ParentalServiceException extends Exception

final case class MovieNotAvailableException(message: String = "The movie is not available due to technical issues.") extends ParentalServiceException