package sky.domain

sealed trait ParentalLevel {
  def name: String
  def weight: Int
}

final case class U(name: String = "U", weight: Int = 100) extends ParentalLevel
final case class PG(name: String = "PG", weight: Int = 200) extends ParentalLevel
final case class Twelve(name: String = "12", weight: Int = 300) extends ParentalLevel
final case class Fifteen(name: String = "15", weight: Int = 400) extends ParentalLevel
final case class Eighteen(name: String = "18", weight: Int = 500) extends ParentalLevel
final case class Undefined(name: String = "Undefined", weight:Int = Int.MaxValue) extends ParentalLevel

object ParentalLevel {
  def apply(level: String): ParentalLevel = {
    level.toLowerCase match {
      case "u" => U()
      case "pg" => PG()
      case "12" => Twelve()
      case "15" => Fifteen()
      case "18" => Eighteen()
      case _ => Undefined(level)
    }
  }
}