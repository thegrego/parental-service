package sky.service

import scala.util.Try

import sky.exception.{TechnicalServiceException, TitleNotFoundException}

trait MovieService {
  @throws[TitleNotFoundException]("The movie cannot be found")
  @throws[TechnicalServiceException]("System error")
  def getParentalControlLevel(movieId: String): Try[String]
}
