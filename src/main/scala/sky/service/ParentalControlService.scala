package sky.service

import scala.util.{Failure, Try}

import sky.domain._
import sky.exception.{MovieNotAvailableException, TechnicalServiceException, TitleNotFoundException}

class ParentalControlService(movieService: MovieService) {
  def movieAllowed(customerParentalLevel: String, movieId: String): Try[Boolean] = {
    movieService.getParentalControlLevel(movieId) map {
      movieParentalLevel => ParentalLevel(customerParentalLevel).weight >= ParentalLevel(movieParentalLevel).weight
    } recoverWith {
      case e: TitleNotFoundException => Failure(e)
      case e: TechnicalServiceException => Failure(MovieNotAvailableException())
      case t: Throwable => Failure(t)
    }
  }
}
